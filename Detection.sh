#!/bin/bash

dir_shower="../../../Showers"
dir_output="../../Data/Reconstruct"

for model in eposlhc sibyll qgsjet
do
	for mass in proton 
	do
		for energy in e19_00 
		do 
			for ang in t60 
			do 
				mkdir -p ${dir_output}/${model}/${mass}/${energy}/${ang}/
				for i in `seq -w 000 009`
				do
					if [ -f "${dir_shower}/${model}/${mass}/${energy}/${ang}/DAT000${i}" ]
					then
						if [ ! -f "${dir_output}/${model}/${mass}/${energy}/${ang}/${model}-${mass}-${energy}-${ang}-DAT000${i}.rec" ]
						then
							echo "Reconstruct shower ${model}, ${mass}, ${energy}, ${ang}, DAT000${i}."	
								./Detection ${dir_shower} ${model} ${mass} ${energy} ${ang} DAT000${i}
								mv ${dir_output}/${model}-${mass}-${energy}-${ang}-DAT000${i}.rec \
								   ${dir_output}/${model}/${mass}/${energy}/${ang}/	
						else
							echo "Shower ${model}, ${mass}, ${energy}, ${ang}, DAT000${i} already reconstruct "
						fi	
					else 
						echo "File ${dir_shower}/${model}/${mass}/${energy}/${ang}/DAT000${i} do not exist!"
					fi
				done
			done
		done
	done
done

