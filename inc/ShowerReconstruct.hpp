#ifndef _SHOWER_RECONSTRUCT_HPP_
#define _SHOWER_RECONSTRUCT_HPP_

#include "TFile.h"
#include "TH2F.h"
#include "Array.hpp"

struct ReconstructData 
{
public:
    double* signal;
    double* error;
    double* pos_x;
    double* pos_y;
    double* time;
    TH2F* muon_map;
    size_t n_detectors;

};

void ReconstructN19(Array& array, double& value, double& error);

void ReadMap(std::string FileName);
void MaximumLikelihood_N19 (Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag);
void ChiSquareAng (Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag);
void ReconstructAng(Array& array, double_t& t0, double_t& th, double_t& ph, double_t& rc);


#endif /* _SHOWER_RECONSTRUCT_HPP_ */