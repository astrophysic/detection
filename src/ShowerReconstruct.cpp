#include "ShowerReconstruct.hpp"
#include "TMinuit.h"

ReconstructData data;

void ReadMap(string FileName){
  //Loading muon maps reference
  TFile* MapFile = new TFile(FileName.c_str());
  data.muon_map =  (TH2F*) MapFile->Get("Muon2D");
  data.muon_map->SetDirectory(0);
  MapFile->Close();
}

void ReconstructN19(Array& array, double& value, double& error)
{
        
    data.n_detectors = array.GetSize();
    data.pos_x = new double[data.n_detectors];
    data.pos_y = new double[data.n_detectors];
    data.signal = new double[data.n_detectors];
    data.error = new double[data.n_detectors];
    data.time = new double[data.n_detectors];
    
    array.GetData(data.pos_x, data.pos_y, data.signal, data.error);

    size_t n_par=1;
    double out_parameters[n_par];
    double out_errors[n_par];

    TMinuit *gMinuit = new TMinuit(n_par);
	gMinuit->SetFCN(MaximumLikelihood_N19);
	gMinuit->Command("SET PRINT -1");

    double parameters[n_par];
    double step_size[n_par];
    double min_value[n_par];            // minimum bound on parameter
    double max_value[n_par];            // maximum bound on parameter
    std::string par_name[n_par]; 

    parameters[0] = 1.0;
    step_size[0] = 0.01;
    min_value[0] = 0;            // minimum bound on parameter
    max_value[0] = 5;            // maximum bound on parameter
    par_name[0] = "N19"; 

    for (size_t i=0; i<n_par; i++){
        gMinuit->DefineParameter(i, par_name[i].c_str(),
        parameters[i], step_size[i], min_value[i], max_value[i]);
	}
//Definindos as flags de execução
	Double_t arglist[10];
		 Int_t ierflg = 0;
	arglist[0] = 1000;
	arglist[1] = 1.;

    //Executando a minimização da função
    gMinuit->mnexcm("MINIMIZE", arglist, 2, ierflg);
    gMinuit->mnexcm("MIGRAD", arglist, 2, ierflg);
    // gMinuit->mnexcm("MINOS", arglist, 0, ierflg);

    // std::cout << "foi minimiza" << '\n';

    for (int i=0; i<n_par; i++){
        gMinuit->GetParameter(i,out_parameters[i],out_errors[i]);
    }

    value = out_parameters[0];
    error = out_errors[0];
    delete gMinuit;
    delete data.pos_x;
    delete data.pos_y;
    delete data.error;
    delete data.signal;
}


void MaximumLikelihood_N19 (Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag){
    double log_ll = 0;
    double lambda;
	for (int i=0; i<data.n_detectors; i++){
		lambda = par[0]*(M_PI*1.8*1.8)*data.muon_map->Interpolate(data.pos_x[i],data.pos_y[i]);
		log_ll += -data.signal[i]*log(lambda)+lambda;
	}
		f=log_ll;
}


void ReconstructAng(Array& array, double_t& t0, double_t& th, double_t& ph, double_t& rc){

     
    data.n_detectors = array.GetSize();
    data.pos_x = new double[data.n_detectors];
    data.pos_y = new double[data.n_detectors];
    data.signal = new double[data.n_detectors];
    data.error = new double[data.n_detectors];
    data.time = new double[data.n_detectors];
    
    array.GetData(data.pos_x, data.pos_y, data.signal, data.error, data.time);

  //Setting up TMinuit
  int n_parameters=4;
  double out_parameters[n_parameters];
  double out_errors[n_parameters];

  //Ajustando número de parametros e a função
  TMinuit *gMinuit = new TMinuit(n_parameters);
  gMinuit->SetFCN(ChiSquareAng);
  gMinuit->Command("SET PRINT -1");

  //std::cout << "Fit: " << SF_T0 << " " << SF_TH << " " << SF_PH << '\n';

  //Definindo os valores iniciais de ajuste
  double parameters[n_parameters];               // the start values
  double step_size[n_parameters];          // step sizes
  double min_value[n_parameters];            // minimum bound on parameter
  double max_value[n_parameters];            // maximum bound on parameter
  std::string parameters_name[n_parameters];

  parameters[0] = 740000;            // a guess
  step_size[0] = 0.001;       // take e.g. 0.1 of start value
  min_value[0] = 0;   // if min and max values = 0, parameter is unbounded.
  max_value[0] = 0;
  parameters_name[0] = "T0";

  parameters[1] = 1.0;            // a guess
  step_size[1] = 0.001;       // take e.g. 0.1 of start value
  min_value[1] = -10;   // if min and max values = 0, parameter is unbounded.
  max_value[1] = 10;
  parameters_name[1] = "u";

  parameters[2] = 1.0;            // a guess
  step_size[2] = 0.001;       // take e.g. 0.1 of start value
  min_value[2] = -10;   // if min and max values = 0, parameter is unbounded.
  max_value[2] = 10;
  parameters_name[2] = "v";

  // par[3] = 10000*2*0.3;            // a guess
  parameters[3] = 1/(10000*2*0.3);            // a guess
  step_size[3] =0.001;       // take e.g. 0.1 of start value
  min_value[3] = 0;   // if min and max values = 0, parameter is unbounded.
  max_value[3] = 0;
  parameters_name[3] = "R";

  for (int i=0; i<n_parameters; i++){
		gMinuit->DefineParameter(i, parameters_name[i].c_str(),
			parameters[i], step_size[i], min_value[i], max_value[i]);
	}

	//Definindos as flags de execução
	Double_t arglist[10];
	Int_t ierflg = 0;
	arglist[0] = 500;
	arglist[1] = 1.;

	//Executando a minimização da função
	gMinuit->mnexcm("MINIMIZE", arglist, 2, ierflg);
	// gMinuit->mnexcm("MIGRAD", arglist, 2, ierflg);
	gMinuit->mnexcm("MINOS", arglist, 2, ierflg);

	for (int i=0; i<n_parameters; i++){
		gMinuit->GetParameter(i,out_parameters[i],out_errors[i]);
	}

  double us, vs, rs;

  us = -1*out_parameters[1]*0.3;
  vs = -1*out_parameters[2]*0.3;
  rs =  1*out_parameters[3]*0.3;

  t0 = out_parameters[0];
  th = 180*asin(sqrt(us*us+vs*vs))/M_PI;
  ph = 180*atan2(vs,us)/M_PI;
  rc = 1.0/(2*rs);
//   if (ph < 0 )
//     ph+=360;

  
  delete gMinuit;
  delete data.pos_x;
  delete data.pos_y;
  delete data.error;
  delete data.signal;
  delete data.time;
}

void ChiSquareAng (Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag){
   double chisq = 0;
   double sigt=25.*25.;
   double dt;
   double u = sin(60*M_PI/180)*cos(0*M_PI/180);
   double v = sin(60*M_PI/180)*sin(0*M_PI/180);

   for (int i=0; i<data.n_detectors; i++){
     double X = data.pos_x[i];
     double Y = data.pos_y[i];

     if (data.signal[i] > 0){
      //  double rp = u*X + v*Y;
       double rp = par[1]*0.3*X + par[2]*0.3*Y;
       double rho = sqrt(X*X+Y*Y);
       double delta = (rho*rho - rp*rp);

       //cerr << "dt: "<<  rp << " " << rho  << " " << delta/0.3 << endl;

      dt =  data.time[i] - (par[0] + par[1]*X + par[2]*Y + delta*par[3]);
     }
     chisq += dt*dt/sigt;
   }
   f = chisq;
}