#include <iostream>
#include <string>
#include <omp.h>

#include "CorsikaBinary.hpp"
#include "Array.hpp"
#include "Dethinning.hpp"
#include "ShowerReconstruct.hpp"
#include "H1D.hpp"


main(int argc, char const *argv[])
{
    // Lendo variável de entrada
    string input_file = string(argv[1])+ "/" +   // diretório
                        string(argv[2])+ "/" +   // modelo
                        string(argv[3])+ "/" +   // massa
                        string(argv[4])+ "/" +   // energia
                        string(argv[5])+ "/" +   // ângulo
                        string(argv[6]);         // arquivo


    double value, error;
    double t0, theta, phi, rc;

    Array array;

    ReadMap("map/EPOSLHC-19_00-P-T60-DeThinn.root");

    CorsikaBinary ShowerSibyll(input_file);
    array.GenerateCore();
    while (ShowerSibyll.NextShower())
    {
        ParticleArray particles = ShowerSibyll.GetParticles();       
        CorsikaParticle primary = ShowerSibyll.GetPrimary();

        #pragma omp parallel for
        for(size_t i = 0; i < particles.size(); i++)
        {
            CorsikaParticle* current = particles.at(i);
            int type = current->GetType();

            if (type == 5 || type == 6)
            {
                // Aplicando o processo de dethinning
                ParticleArray secondaries;
                Dethinnig(&primary,current,&secondaries);

                array.Crossed(secondaries);

                for (auto& secondary : secondaries)
                {
                    delete secondary;
                }
            }  
        }
    }

    std::string out_file = "../../Data/Reconstruct/" +
                            string(argv[2])+ "-" +   // modelo
                            string(argv[3])+ "-" +   // massa
                            string(argv[4])+ "-" +   // energia
                            string(argv[5])+ "-" +   // ângulo 
                            string(argv[6])+ ".rec";
    array.Print(out_file);

    return 0;
}
